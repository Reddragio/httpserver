# HTTPServer

## The project

This HTTP allow you to serve every file type (dynamic content type is implemented). It handles GET, PUT, POST, HEAD and DELETE. 
You can upload files through the index.html page. You can also delete them.  

## Project structure

The project contains 2 main directories: **resources** and **src**

**resources** contains the resources the http server need to serve (index.html, style.css, few html pages and the upload directory).

**src** contains the sources.
- Webserver.java contains the main method, it handle new connexions, and create a ReceiveThread for each new client.
- Database.java handles the connexion with the sqlite database (used to store the user/password)
- HTTPRequestHandling contains all that is related to the HTTP Request

## Environment setup

To launch the project, you need to add the SQLITE library to communicate with the database