package http.server.HTTPRequestHandling;

import java.io.FileNotFoundException;
import java.io.OutputStream;
import java.io.PrintWriter;

/**
 * class whose role is to execute a GET request
 * @author Jacques CHARNAY & Hugo REYMOND
 */
public class RequestExecutorGET extends RequestExecutor {

    /**
     * Send the file requested by the client.
     * @param req The client request
     * @param out The client output stream
     */
    @Override
    public void execute(HTTPRequest req, OutputStream out) {
        // Removing the /
        String fileUrl = req.getUrl().substring(1);
        System.out.println(fileUrl);
        PrintWriter printWriter = new PrintWriter(out);

        if (!fileUrl.equals("")) {
            if(fileUrl.indexOf("..") != -1){
                //forbidden operation
                sendHeaders(printWriter,null,StatusCode.FORBIDDEN);
            }
            else{
                try {
                    // Send the response
                    // Send the headers
                    sendResource(out, fileUrl,StatusCode.OK);
                } catch (FileNotFoundException e) {
                    // File not found
                    sendHeaders(printWriter,null,StatusCode.NOT_FOUND);
                    printWriter.println("");
                    printWriter.println("<H1>FILE NOT FOUND</H1>");
                    printWriter.flush();
                }
            }
        } else {
            // Send the default response
            try {
                sendResource(out, "index.html",StatusCode.OK);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
        }
    }



}
