/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package http.server.HTTPRequestHandling;

import java.util.HashMap;

/**
 * Represent an HTTPRequest and all his essential fields
 * @author Jacques CHARNAY & Hugo REYMOND
 */
public class HTTPRequest {

    protected String type;
    protected String url;
    protected Integer content_length;
    protected String host;
    protected HashMap<String,String> request_params;
    protected String content_type;
    protected String body;
    protected byte[] rawRequest;

    /*** HttpRequest constructor
     *
     * Initialize the parameters Hashmap and set the content_length to null
     */
    public HTTPRequest() {
        request_params = new HashMap<>();
        content_length = null;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public int getContent_length() {
        return content_length;
    }

    public void setContent_length(int content_length) {
        this.content_length = content_length;
    }

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public HashMap<String, String> getRequest_params() {
        return request_params;
    }

    public void setRequest_params(HashMap<String, String> request_params) {
        this.request_params = request_params;
    }

    public String getContent_type() {
        return content_type;
    }

    public void setContent_type(String content_type) {
        this.content_type = content_type;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getBody() {
        return body;
    }

    public byte[] getRawRequest() {
        return rawRequest;
    }

    public void setBody(String body) {
        this.body = body;
    }

    @Override
    public String toString() {
        return "Request{" + "type=" + type + ", content_length=" + content_length + ", host=" + host + ", request_params=" + request_params + ", content_type=" + content_type + '}';
    }
    
}
