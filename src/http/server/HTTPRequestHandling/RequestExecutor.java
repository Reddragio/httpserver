package http.server.HTTPRequestHandling;

import http.server.WebServer;

import java.io.*;
import java.nio.file.Files;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Abstract class whose role is to execute a request
 * @author Jacques CHARNAY & Hugo REYMOND
 */
public abstract class RequestExecutor {

    /**
     * Execute the given request and send the respond over the OutputStream
     * @param request
     * @param out
     */
    abstract void execute(HTTPRequest request, OutputStream out);

    /**
     * Send the given resource to the client over the OutputStream, with the desired status code
     * @param out the Output stream of the client
     * @param resourceUrl the url the client want to reach
     * @param statusCode the status code to send
     * @throws FileNotFoundException
     */
    public static void sendResource(OutputStream out, String resourceUrl, int statusCode) throws FileNotFoundException {
        // Opening the file
        File file = new File(WebServer.resourceFolder + resourceUrl);
        FileInputStream fos = new FileInputStream(file);

        // Sending the headers
        sendHeaders(new PrintWriter(out), file, statusCode);
        try {
            byte[] fileBytes = new byte[(int) file.length()];
            fos.read(fileBytes, 0, (int) file.length());
            out.write(fileBytes);
        } catch (IOException ex) {
            Logger.getLogger(WebServer.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    /**
     * Send the response header, with the desired status code.
     * It is possible to eventually include information about a given file.
     * @param out the Output stream of the client
     * @param file the file we send to the client, use to determine Content-type
     * @param statusCode the status code to send
     */
    public static void sendHeaders(PrintWriter out, File file, int statusCode) {
        // Send the headers
        out.println("HTTP/1.0 "+ statusCode +" OK");

        if(file != null){
            try {
                out.println("Content-Type: " + Files.probeContentType(file.toPath()));
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        else{
            out.println("Content-Type: text/html");
        }

        out.println("Server: Bot");
        // this blank line signals the end of the headers
        out.println("");
        out.flush();
    }
}
