package http.server.HTTPRequestHandling;

import http.server.WebServer;

import java.io.File;
import java.io.OutputStream;
import java.io.PrintWriter;

/**
 * class whose role is to execute a DELETE request
 * @author Jacques CHARNAY
 */
public class RequestExecutorDELETE extends RequestExecutor {
    private static String deleteFolder = "/upload";

    @Override
    public void execute(HTTPRequest request, OutputStream out) {
        String url = WebServer.resourceFolder + deleteFolder + request.getUrl();
        if(url.indexOf("..") == -1 && deleteFile(url)){
            sendHeaders(new PrintWriter(out),null,StatusCode.OK);
        }
        else{
            sendHeaders(new PrintWriter(out),null,StatusCode.BAD_REQUEST);
        }
    }

    /**
     * Delete the file at url
     * @param url
     * @return
     */
    private boolean deleteFile(String url){
        File file = new File(url);
        return file.delete();
    }
}
