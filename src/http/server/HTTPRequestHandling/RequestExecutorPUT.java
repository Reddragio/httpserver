package http.server.HTTPRequestHandling;

import http.server.WebServer;

import java.io.FileNotFoundException;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.HashMap;

/**
 * class whose role is to execute a PUT request
 * @author Jacques CHARNAY
 */
public class RequestExecutorPUT extends RequestExecutor {
    @Override
    public void execute(HTTPRequest request, OutputStream out) {
        updatePassword(request,out);
    }

    /**
     * PUT request to update the password
     * @param req
     * @param out
     */
    private void updatePassword(HTTPRequest req, OutputStream out){
        String body = req.getBody();
        HashMap<String,String> params = new HashMap<>();
            if(body!=null){
            for(String param :  body.split("&")){
                params.put(param.split("=")[0], param.split("=")[1]);
            }
        }else{
            PrintWriter printWriter = new PrintWriter(out);
            sendHeaders(printWriter,null,StatusCode.BAD_REQUEST);
        }

        if(params.containsKey("pseudo")&&params.containsKey("old_password")&&params.containsKey("new_password")){
            if(WebServer.getDatabase().checkIfPasswordIsValid(params.get("pseudo"),params.get("old_password"))){
                try {
                    WebServer.getDatabase().updatePassword(params.get("pseudo"),params.get("new_password"));
                    sendResource(out, "success.html",StatusCode.OK);
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                }
            }else{
                System.out.println("Mauvais login/mdp");
                try {
                    sendResource(out, "wrongPassword.html",StatusCode.OK);
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
