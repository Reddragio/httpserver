package http.server.HTTPRequestHandling;

import java.io.*;
import java.util.Arrays;

/**
 * @author Jacques CHARNAY & Hugo REYMOND
 */
public class HTTPRequestFactory {

    /**
     * Parse an http request and build a convenient HTTPRequest object
     *
     * @param isInit
     * @return the parsed http request
     * @throws IOException
     */
    public static HTTPRequest parseRequest(DataInputStream isInit) throws IOException {
        // We create an empty request
        HTTPRequest req = new HTTPRequest();

        while (isInit.available() == 0) {
            //Sleep until data are sent
            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        int newByte;
        ByteArrayOutputStream byteArrayStream = new ByteArrayOutputStream();
        while (isInit.available() > 0) {
            newByte = isInit.read();
            byteArrayStream.write(newByte);
        }
        req.rawRequest = byteArrayStream.toByteArray();
        byte[] copy = Arrays.copyOfRange(req.rawRequest, 0, req.rawRequest.length);
        InputStream is = new ByteArrayInputStream(copy);

        InputStreamReader reader = new InputStreamReader(is);
        BufferedReader in = new BufferedReader(reader);
        String str = ".";
        // We isolate the first line, and split it to get the type,url and version in 3 Strings
        String[] first = in.readLine().split(" ");

        // If the first line has more than 2 arguments ( RequestType + URL + HTTPVersion)
        if (first.length > 2) {
            // We set the request type
            req.setType(first[0]);

            // We analyse the URL, does it contains parameters ?
            if (first[1].contains("?")) {
                // Set the url
                req.setUrl(first[1].split("\\?")[0]);
                // Set the params
                for (String param : first[1].split("\\?")[1].split("&")) {
                    req.request_params.put(param.split("=")[0], param.split("=")[1]);
                }
            } else {
                // Set the url
                req.url = first[1];
            }

            // Reading header
            while (!str.equals("")) {
                str = in.readLine();
                System.out.println(str);
                if (str.contains("Content-Type: ")) {
                    req.content_type = str.substring(str.indexOf("Content-Type: ") + "Content-Type: ".length());
                }
                if (str.contains("Host: ")) {
                    req.host = str.substring(str.indexOf("Host: ") + "Host: ".length());
                }
                if (str.contains("Content-Length: ")) {
                    req.content_length = Integer.parseInt(str.substring(str.indexOf("Content-Length: ") + "Content-Length: ".length()));
                }


            }
            // If there is a body
            if (req.content_length != null) {
                char[] data = new char[req.content_length];
                int i = 0;
                while (in.ready()) {
                    data[i++] = (char) in.read();
                }
                req.body = new String(data);
            }

        } else {
            throw new IOException("Requête invalide !");
        }

        return req;
    }

}
