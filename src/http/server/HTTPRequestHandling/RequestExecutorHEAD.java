package http.server.HTTPRequestHandling;

import http.server.WebServer;

import java.io.File;
import java.io.OutputStream;
import java.io.PrintWriter;

/**
 * class whose role is to execute a HEAD request
 * @author Hugo REYMOND
 */

public class RequestExecutorHEAD extends RequestExecutor {

    @Override
    public void execute(HTTPRequest req, OutputStream out){
        String fileUrl = req.getUrl().substring(1);
        File file = new File(WebServer.resourceFolder+fileUrl);
        PrintWriter printWriter = new PrintWriter(out);
        if(file.exists()){
            sendHeaders(printWriter,file,StatusCode.OK);
        }
        else{
            sendHeaders(printWriter,file,StatusCode.NOT_FOUND);
        }
    }
}
