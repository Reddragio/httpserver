package http.server.HTTPRequestHandling;

import http.server.WebServer;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.util.Arrays;
import java.util.HashMap;

/**
 * class whose role is to execute a POST request
 * @author Jacques CHARNAY & Hugo REYMOND
 */
public class RequestExecutorPOST extends RequestExecutor {

    /**
     * Catch POST request from login or index pages
     * @param req
     * @param out
     */
    @Override
    public void execute(HTTPRequest req, OutputStream out) {
        System.out.println("URL=="+req.getUrl());
        if(req.getUrl().equals("/login.html")){
            login(req,out);
        }
        else if(req.getUrl().equals("/index.html")){
            upload(req,out);
        }
    }

    /**
     * Upload a file to server
     * @param req
     * @param out
     */
    private void upload(HTTPRequest req, OutputStream out){
        String body = req.getBody();
        String fileName = getFileName(body);
        System.out.println(fileName);
        System.out.println("Upload en cours...");

        File file = new File(WebServer.resourceFolder + "/upload/"+fileName);
        try {
            FileOutputStream fos = new FileOutputStream(file);
            byte[] rawFile = getFileRawData(req);
            fos.write(rawFile);
            fos.close();

            //File successfully upload:
            sendResource(out,"index.html",StatusCode.OK);

        } catch (Exception e) {
            e.printStackTrace();

            //Bug during upload:
            try {
                sendResource(out,"index.html",StatusCode.BAD_REQUEST);
            } catch (FileNotFoundException ex) {
                ex.printStackTrace();
            }
        }
    }

    /**
     * find the name of the file uploaded
     * @param body
     * @return
     */
    private String getFileName(String body){
        int indexName = body.indexOf("filename=");
        int debut= indexName+10;
        int fin = body.indexOf('\"',debut);
        return body.substring(debut,fin);
    }

    /**
     * Return the raw data of uploaded file
     * @param req
     * @return raw data
     */
    private byte[] getFileRawData(HTTPRequest req){
        byte[] reqRawData = req.getRawRequest();
        int endLine1 = find2EndOfLines(reqRawData,0);
        int endLine2 = find2EndOfLines(reqRawData,endLine1+2);
        int endLine3 = find2EndOfLines(reqRawData,endLine2+2);
        int begin = endLine3+4;
        int end = findDashEnd(reqRawData,begin);

        return Arrays.copyOfRange(reqRawData,begin,end);
    }

    /**
     * Return the index where '\r\n' can be found in array
     * @param array
     * @param from
     * @return
     */
    private int find2EndOfLines(byte[] array, int from){
        for(int i=from;i<array.length-3;++i) {
            if (array[i] == '\r' && array[i + 1] == '\n' && array[i + 2] == '\r' && array[i + 3] == '\n') {
                return i;
            }
        }
        return -1;
    }

    /**
     * Return the index where '-----' can be found in array
     * @param array
     * @param from
     * @return
     */
    private int findDashEnd(byte[] array, int from){
        for(int i=from;i<array.length-4;++i) {
            if (array[i] == '-' && array[i + 1] == '-' && array[i + 2] == '-' && array[i + 3] == '-' && array[i + 4] == '-') {
                return i;
            }
        }
        return -1;
    }

    /**
     * POST request from user for trying to login
     * @param req
     * @param out
     */
    private void login(HTTPRequest req, OutputStream out){
        String body = req.getBody();
        HashMap<String,String> params = new HashMap<>();
        for(String param :  body.split("&")){
            params.put(param.split("=")[0], param.split("=")[1]);
        }
        if(params.containsKey("pseudo")&&params.containsKey("password")){
            if(WebServer.getDatabase().checkIfPasswordIsValid(params.get("pseudo"),params.get("password"))){
                System.out.println("Connexion réussie");
                try {
                    sendResource(out, "/success.html",StatusCode.OK);
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                }
            }else{
                System.out.println("Mauvais login/mdp");
                try {
                    sendResource(out, "wrongPassword.html",StatusCode.OK);
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
