package http.server.HTTPRequestHandling;

/**
 * @author Jacques CHARNAY
 */
public class StatusCode {
    public final static int OK = 200;
    public final static int BAD_REQUEST = 400;
    public final static int FORBIDDEN = 403;
    public final static int NOT_FOUND = 404;
    public final static int INTERNAL_ERROR = 500;
    public final static int NOT_IMPLEMENTED = 501;
}
