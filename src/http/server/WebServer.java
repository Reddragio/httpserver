package http.server;

import java.io.IOException;
import java.net.ServerSocket;

/**
 * @author Jacques CHARNAY & Hugo REYMOND
 */
public class WebServer {
    private static Database database;
    public static String resourceFolder = "resource/";

    /**
     * Start the WebServer.
     * Wait and respond to client connections.
     */
    protected void start() {
        ServerSocket s;

        System.out.println("Webserver starting up on port 3000");
        try {
            // create the main server socket
            s = new ServerSocket(3000);
        } catch (Exception e) {
            System.out.println("Error: " + e);
            return;
        }

        System.out.println("Waiting for connection");
        while(true) {
            try {
                //Create thread for clients connections
                Thread thread = new ReceiveThread(s.accept());
                thread.start();
            } catch (IOException e) {
                e.printStackTrace();
            }

        }
    }

    /**
     * get the actual database
     * @return actual database
     */
    public static Database getDatabase() {
        return database;
    }

    public static void main(String[] args) {
        //We create the database
        database = new Database("user.db");

        if(!database.checkIfPasswordIsValid("Dragio","1234")){
            //We create an user, in order to test login
            database.createUser("Dragio","1234");
        }

        //We start the server
        WebServer ws = new WebServer();
        ws.start();
    }

}
