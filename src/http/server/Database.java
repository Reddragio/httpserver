package http.server;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author Jacques CHARNAY
 */
public class Database {
    private String databaseUrl;
    private Connection conn;

    /**
     * Code to create a database
     * Found on: https://www.sqlitetutorial.net/sqlite-java/create-database/
     * @param databaseFileName
     */
    public Database(String databaseFileName) {
        databaseUrl = "jdbc:sqlite:" + databaseFileName;

        try{
            conn = DriverManager.getConnection(databaseUrl);
            if (conn != null) {
                DatabaseMetaData meta = conn.getMetaData();
                System.out.println("The driver name is " + meta.getDriverName());
                System.out.println("Connexion established with databse");
                createTables(conn);
                System.out.println("Tables created (if they not exist)");
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }

    /**
     * Execute a SQL Query, with optional parameters
     * @param query Query
     * @param parameters parameters corresponding to query
     * @param result True if the query return a result, false otherwise
     * @return the query result, or null otherwise
     */
    private ResultSet executeQuery(String query,String[] parameters,boolean result){
        try {
            PreparedStatement pstmt  = conn.prepareStatement(query);
            for(int i=1;i<=parameters.length;++i){
                pstmt.setString(i,parameters[i-1]);
            }

            if(result){
                ResultSet rs = pstmt.executeQuery();
                return rs;
            }
            else{
                pstmt.execute();
                return null;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return null;
    }

    /**
     * Create the user table
     * @param conn
     * @throws SQLException
     */
    private void createTables(Connection conn) throws SQLException {
        // SQL statement for creating the user table
        String sql = "CREATE TABLE IF NOT EXISTS user (\n"
                + "    id integer PRIMARY KEY AUTOINCREMENT,\n"
                + "    pseudo text NOT NULL,\n"
                + "    password test NOT NULL\n"
                + ");";

        executeQuery(sql,new String[0],false);
    }

    /**
     * Create a new user
     * @param pseudo
     * @param password
     */
    public void createUser(String pseudo, String password){
        String query = "INSERT INTO user(pseudo,password) "
                +"VALUES(?,?);";
        String[] parameters = new String[2];
        parameters[0] = pseudo;
        parameters[1] = password;

        executeQuery(query,parameters,false);
    }

    /**
     * Delete an user
     * @param pseudo
     */
    public void deleterUser(String pseudo){
        String query = "DELETE FROM user " +
                "WHERE pseudo=?;";
        String[] parameters = new String[1];
        parameters[0] = pseudo;

        executeQuery(query,parameters,false);
    }

    /**
     * Update user password
     * @param pseudo
     * @param password
     */
    public void updatePassword(String pseudo,String password){
        String query = "UPDATE user " +
                "SET password=? " +
                "WHERE pseudo=?;";
        String[] parameters = new String[2];
        parameters[0] = password;
        parameters[1] = pseudo;

        executeQuery(query,parameters,false);
    }

    public List<String> getUsersList(){
        String query = "SELECT pseudo FROM user;";

        ResultSet res = executeQuery(query,new String[0],true);
        List<String> usersList = new ArrayList<>();

        try{
            while (res.next()) {
                usersList.add(res.getString("pseudo"));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return usersList;
    }

    /**
     * @return all users and password (for admin)
     */
    public List<String> getUsersWithPasswordList(){
        String query = "SELECT pseudo,password FROM user;";

        ResultSet res = executeQuery(query,new String[0],true);
        List<String> usersList = new ArrayList<>();

        try{
            while (res.next()) {
                usersList.add(res.getString("pseudo")+" - "+res.getString("password"));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return usersList;
    }

    /**
     * Check if a password is correct for a given user
     * @param pseudo
     * @param password
     * @return true this is the good password, false otherwise
     */
     public boolean checkIfPasswordIsValid(String pseudo, String password){
        String query = "SELECT count(*) as nb FROM user " +
                "WHERE pseudo=? AND password=?;";

        String[] parameters = new String[2];
        parameters[0] = pseudo;
        parameters[1] = password;

        ResultSet set = executeQuery(query,parameters,true);
        Integer nb_users = 0;
        try {
            nb_users = set.getInt("nb");
        } catch (SQLException ex) {
            Logger.getLogger(Database.class.getName()).log(Level.SEVERE, null, ex);
        }

        return nb_users >= 1;
    }

}
