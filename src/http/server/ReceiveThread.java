package http.server;

import http.server.HTTPRequestHandling.*;

import java.io.DataInputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.Socket;

/**
 * @author Jacques CHARNAY & Hugo REYMOND
 */
public class ReceiveThread extends Thread {
    private Socket remote;
    private static RequestExecutorGET requestExecutorGET = new RequestExecutorGET();
    private static RequestExecutorPOST requestExecutorPOST = new RequestExecutorPOST();
    private static RequestExecutorPUT requestExecutorPUT = new RequestExecutorPUT();
    private static RequestExecutorDELETE requestExecutorDELETE = new RequestExecutorDELETE();
    private static RequestExecutorHEAD requestExecutorHEAD = new RequestExecutorHEAD();

    public ReceiveThread(Socket remote) {
        this.remote = remote;
    }

    /**
     * receives a request from client and try to respond
     */
    public void run() {
        try {
            // remote is now the connected socket
            System.out.println("Connection, sending data.");

            OutputStream outputStream = remote.getOutputStream();
            PrintWriter out = new PrintWriter(outputStream);

            try {
                // We parse the request:
                HTTPRequest req = HTTPRequestFactory.parseRequest(new DataInputStream(remote.getInputStream()));
                // We respond to the request:
                switch (req.getType()) {
                    case "GET":
                        requestExecutorGET.execute(req, outputStream);
                        break;
                    case "POST":
                        requestExecutorPOST.execute(req, outputStream);
                        break;
                    case "PUT":
                        requestExecutorPUT.execute(req, outputStream);
                        break;
                    case "DELETE":
                        requestExecutorDELETE.execute(req, outputStream);
                        break;
                    case "HEAD":
                        requestExecutorHEAD.execute(req, outputStream);
                        break;
                    default:
                        RequestExecutor.sendHeaders(out, null, StatusCode.NOT_IMPLEMENTED);
                        break;
                }
            }catch (Exception e){
                // If we catch an error, send  a 500 request
                RequestExecutor.sendResource(remote.getOutputStream(),"error.html",StatusCode.INTERNAL_ERROR);
            }
            outputStream.flush();
            remote.close();
        } catch (Exception e) {
            System.out.println("Error: " + e);
        }
    }

}
